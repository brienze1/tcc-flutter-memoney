import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/device.dart';
import 'package:memoney/br/com/memoney/domain/entity/user.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/user_dao.dart';
import 'package:memoney/br/com/memoney/integration/dto/user_dto.dart';
import 'package:memoney/br/com/memoney/integration/restclient/new_device_rest_client.dart';
import 'package:memoney/br/com/memoney/integration/restclient/new_user_rest_client.dart';
import 'package:memoney/br/com/memoney/integration/utils/jwt_converter.dart';
import 'package:uuid/uuid.dart';

import 'login_service.dart';

Future<bool> register(BuildContext context, String firstName, String lastName, String email, String password) async {
  final String payload = "payload";

  try {
    String registrationToken = await newUserRestClient(
        context,
        UserDto(
            firstName: firstName,
            lastName: lastName,
            email: email.toLowerCase(),
            password: password,
            device: Device(deviceId: Uuid().v1())));
    Map<String, dynamic> mapaUsuario = decodeWithoutVerification(registrationToken)[payload];
    UserDto userDto = UserDto.toUserDto(mapaUsuario);
    await UserDao.save(userDto.toUser());
    await login(context, email, password);
  } catch (e) {
    debugPrint("erro ao registrar: " + e.toString());
    return false;
  }
  return true;
}

Future<bool> registerDevice(BuildContext context, String email, String password) async {
  final String payload = "payload";

  try {
    String registrationToken = await newDeviceRestClient(
        context, UserDto(email: email.toLowerCase(), password: password, device: Device(deviceId: Uuid().v1())));
    Map<String, dynamic> mapaUsuario = decodeWithoutVerification(registrationToken)[payload];
    UserDto userDto = UserDto.toUserDto(mapaUsuario);
    await UserDao.save(userDto.toUser());
  } catch (e) {
    debugPrint("erro ao registrar: " + e.toString());
    return false;
  }
  return true;
}
