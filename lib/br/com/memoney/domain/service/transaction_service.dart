import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/integration/restclient/transactions_rest_client.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';

Future<List<Transaction>> getLatestTransactions(BuildContext context,
    {int daysAgo, int quantity, int page}) async {
  DateTime date;
  if (daysAgo != null && daysAgo >= 0) {
    date = DateTimeBrazilia.now().subtract(Duration(days: daysAgo));
  }
  List<Transaction> transactions = await getLastTransactions(context,
      quantity: quantity != null && quantity > 0 ? quantity : null,
      page: page != null && page > 0 ? page : null,
      date: date);
  transactions.sort((a, b) => b.data.compareTo(a.data));

  return transactions;
}

Future<Transaction> registerNewTransaction(
    BuildContext context, Transaction transaction,) async {
  return await postNewTransaction(context, transaction);
}
