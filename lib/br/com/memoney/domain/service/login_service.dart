import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/active_user.dart';
import 'package:memoney/br/com/memoney/domain/entity/user.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/user_dao.dart';
import 'package:memoney/br/com/memoney/integration/restclient/token_rest_client.dart';

Future<bool> hasAutoLogin() async {
  User user = await UserDao.findMainUser();
  if (user != null && user.deviceId != null && user.deviceId.isNotEmpty) {
    return true;
  } else {
    return false;
  }
}

Future<bool> autoLogin() async {
  User user = await UserDao.findMainUser();
  return false;
}

Future<bool> login(BuildContext context, String email, String password) async {
  User user = await UserDao.findByEmail(email.toLowerCase());
  await ActiveUserDao.save(ActiveUser(
      email: email.toLowerCase(),
      firstName: user.firstName,
      lastName: user.lastName,
      password: password,
      token: null,
      secret: user.secret,
      deviceId: user.deviceId));
  String token = await getToken(context);

  return token != null;
}

Future<bool> userIsRegisteredToDevice(String email) async {
  User user = await UserDao.findByEmail(email.toLowerCase());
  return user != null;
}
