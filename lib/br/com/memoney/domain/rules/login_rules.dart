
import 'package:memoney/br/com/memoney/domain/rules/registration_rules.dart';

bool validateLogin(String email, String password){
  if(!validateEmail(email)) {
    return false;
  }
  if(!validatePassword(password)) {
    return false;
  }
  return true;
}