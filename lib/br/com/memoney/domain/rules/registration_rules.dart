bool validateRegistration(String firstName, String lastName, String email, String password, String confirmPassword) {
  if(validateName(firstName) && validateName(lastName) && validateEmail(email) && validatePassword(password) && validateConfirmPassword(password, confirmPassword)) {
    return true;
  } else {
    return false;
  }
}

bool validateName(String name) {
  Pattern pattern = r"^(?=.{1,40}$)[a-zA-Z]+(?:[-'\s][a-zA-Z]+)*$";
  RegExp regex = new RegExp(pattern);
  if(!validate(name)) {
    return false;
  } else if(!regex.hasMatch(name)){
    return false;
  }
  return true;
}

bool validateEmail(String email) {
  Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);

  if(!validate(email)){
    return false;
  }
  if (!regex.hasMatch(email)){
    return false;
  }
  return true;
}

bool validatePassword(String password) {
  return validate(password);
}

bool validateConfirmPassword(String password, String confirmPassword) {
  if(!validatePassword(confirmPassword)){
    return false;
  } else if(password != confirmPassword) {
    return false;
  }
  return true;
}

bool validate(String string) {
  if(string != null && string.trim().isNotEmpty){
    return true;
  } else {
    return false;
  }
}