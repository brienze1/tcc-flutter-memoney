import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';

List<Transaction> _addUpHours(List<Transaction> transactions){
  List<Transaction> addedUpTransactions = List();

  for(int i=25; i>=-1; i--){
    addedUpTransactions.add(Transaction(valor: 0, data: DateTimeBrazilia.now().subtract(Duration(hours: i))));
  }
  for(Transaction transaction in transactions) {
    for(Transaction addedTransaction in addedUpTransactions){
      if(transaction.data.day == addedTransaction.data.day && transaction.data.hour == addedTransaction.data.hour){
        if(transaction.tipo == "Sent") {
          addedTransaction.valor -= transaction.valor;
        } else {
          addedTransaction.valor += transaction.valor;
        }
        if(addedTransaction.valor < 0) {
          addedTransaction.tipo = "Sent";
        }
      }
    }
  }
  return addedUpTransactions;
}

List<Transaction> buildTransactionDataSource(List<Transaction> transactions, String format){
  List<Transaction> addedUpTransactions = List();
  int quantity;
  bool byDays;
  bool byMonths;
  
  if(format == "all") {
    if((transactions.last.data.difference(DateTimeBrazilia.now()).inDays)/30 > 12) {
      quantity = (transactions.last.data.difference(DateTimeBrazilia.now()).inDays)%30+1;
    } else {
      quantity = 12;
    }
    byDays = false;
    byMonths = true;
  } else if(format == "year") {
    quantity = 12;
    byDays = false;
    byMonths = true;
  } else if(format == "month"){
    quantity = 30;
    byDays = true;
    byMonths = false;
  } else if(format == "week"){
    quantity = 7;
    byDays = true;
    byMonths = false;
  } else {
    return _addUpHours(transactions);
  }

  if(byDays != null && byDays){
    for(int i=quantity+1; i>=-1; i--){
      addedUpTransactions.add(Transaction(valor: 0, data: DateTimeBrazilia.now().subtract(Duration(days: i))));
    }
  } else if(byMonths != null && byMonths) {
    for(int i=quantity+1; i>=-1; i--){
      addedUpTransactions.add(Transaction(valor: 0, data:  DateTimeBrazilia.now().subtract(Duration(days: 31*i))));
    }
  }

  for(Transaction transaction in transactions) {
    for(Transaction addedTransaction in addedUpTransactions){
      if(byDays != null && byDays && transaction.data.month == addedTransaction.data.month && transaction.data.day == addedTransaction.data.day){
        if(transaction.tipo == "Sent") {
          addedTransaction.valor -= transaction.valor;
        } else {
          addedTransaction.valor += transaction.valor;
        }
        if(addedTransaction.valor < 0) {
          addedTransaction.tipo = "Sent";
        }
      } else if(byMonths != null && byMonths && transaction.data.year == addedTransaction.data.year && transaction.data.month == addedTransaction.data.month){
        if(transaction.tipo == "Sent") {
          addedTransaction.valor -= transaction.valor;
        } else {
          addedTransaction.valor += transaction.valor;
        }
        if(addedTransaction.valor < 0) {
          addedTransaction.tipo = "Sent";
        }
      }
    }
  }
  addedUpTransactions.first.valor = 0;
  addedUpTransactions.last.valor = 0;

  return addedUpTransactions;
}