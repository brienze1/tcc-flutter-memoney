import 'package:memoney/br/com/memoney/integration/database/dao/user_dao.dart';

class User {
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String secret;
  final String deviceId;
  bool autoLogin;
  bool validEmail;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.secret,
    this.autoLogin,
    this.validEmail,
    this.deviceId,
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> userMap = Map();
    userMap[UserDao.id] = this.id;
    userMap[UserDao.firstName] = this.firstName;
    userMap[UserDao.lastName] = this.lastName;
    userMap[UserDao.email] = this.email;
    userMap[UserDao.secret] = this.secret;
    userMap[UserDao.deviceId] = this.deviceId;
    userMap[UserDao.autoLogin] = this.autoLogin ? "1" : "0";
    userMap[UserDao.validEmail] = this.validEmail ? "1" : "0";
    return userMap;
  }

  static User toUser(Map<String, dynamic> map) {
    return User(
      id: map[UserDao.id],
      firstName: map[UserDao.firstName],
      lastName: map[UserDao.lastName],
      email: map[UserDao.email],
      secret: map[UserDao.secret],
      deviceId: map[UserDao.deviceId],
      autoLogin: map[UserDao.autoLogin] == 1 ? true : false,
      validEmail: map[UserDao.validEmail] == 1 ? true : false,
    );
  }

  @override
  String toString() {
    return 'User{id: $id, firstName: $firstName, lastName: $lastName, email: $email, secret: $secret, deviceId: $deviceId, autoLogin: $autoLogin, verified: $validEmail}';
  }
}
