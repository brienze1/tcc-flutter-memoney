import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';

class ActiveUser {
  final int id;
  final String email;
  final String firstName;
  final String lastName;
  final String password;
  final String secret;
  final String deviceId;
  String token;

  ActiveUser({this.id, this.email, this.password, this.token, this.secret, this.firstName, this.lastName, this.deviceId});

  @override
  String toString() {
    return 'ActiveUser{id: $id, email: $email, firstName: $firstName, lastName: $lastName, password: $password, secret: $secret, deviceId: $deviceId, token: $token}';
  }

  static ActiveUser toActiveUSer(Map<String, dynamic> map) {
    return ActiveUser(
        id: map[ActiveUserDao.id],
        email: map[ActiveUserDao.email],
        firstName: map[ActiveUserDao.firstName],
        lastName: map[ActiveUserDao.lastName],
        password: map[ActiveUserDao.password],
        secret: map[ActiveUserDao.secret],
        deviceId: map[ActiveUserDao.deviceId],
        token: map[ActiveUserDao.token]);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> mapaActiveUser = Map();
    mapaActiveUser[ActiveUserDao.id] = this.id;
    mapaActiveUser[ActiveUserDao.email] = this.email;
    mapaActiveUser[ActiveUserDao.firstName] = this.firstName;
    mapaActiveUser[ActiveUserDao.lastName] = this.lastName;
    mapaActiveUser[ActiveUserDao.password] = this.password;
    mapaActiveUser[ActiveUserDao.secret] = this.secret;
    mapaActiveUser[ActiveUserDao.deviceId] = this.deviceId;
    mapaActiveUser[ActiveUserDao.token] = this.token;

    return mapaActiveUser;
  }
}
