import 'dart:convert';

import 'package:flutter/cupertino.dart';

class Device {
  final int id;
  final String deviceId;

  Device({this.id, this.deviceId});

  @override
  String toString() {
    return 'Device{id: $id, deviceId: $deviceId}';
  }

  String toJson() {
    Map<String, dynamic> mapa = new Map();
    mapa["device_id"] = this.deviceId;
    return jsonEncode(mapa);
  }

  Map<String, dynamic> toMap() {
    Map<String, String> mapa = new Map();
    mapa["device_id"] = this.deviceId;
    return mapa;
  }

  static Device toDevice(Map<String, dynamic> deviceMap) {
    return Device(
      id: deviceMap["id"],
      deviceId: deviceMap["device_id"],
    );
  }
}
