import 'dart:convert';

import 'package:intl/intl.dart';

class Transaction {
  String id;
  double valor;
  DateTime data;
  String descricao;
  String tipo;

  Transaction({this.id, this.valor, this.data, this.descricao, this.tipo});

  @override
  String toString() {
    return 'Transaction{id: $id, valor: $valor, data: $data, descricao: $descricao, tipo: $tipo}';
  }

  static Transaction fromJson(Map<String, dynamic> json) {
    return Transaction(
        id: json["id"],
        valor: double.tryParse(json["valor"].toString()),
        data: DateTime.parse(json["data"]),
        tipo: json["tipo"],
        descricao: json["descricao"]);
  }

  String toJson() {
    Map<String, String> json = new Map();
    json["valor"] = this.valor.toString();
    json["data"] = DateFormat("yyyy-MM-ddTHH:mm:ss.SSS").format(this.data);
    json["tipo"] = this.tipo.toString();
    json["descricao"] = this.descricao.toString();

    return jsonEncode(json);
  }
}
