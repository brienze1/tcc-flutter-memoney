import 'package:http_interceptor/http_interceptor.dart';

class LoggingInterceptorBuilder implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    print("----- Request Start -----");
    print("URL: " + data.url);
    print("Headers: " + data.headers.toString());
    print("Body: " + data.body);
    print("----- Request End -----");
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    print("----- Response Start -----");
    print("Status: " + data.statusCode.toString());
    print("Headers: " + data.headers.toString());
    print("Body: " + data.body);
    print("----- Response End -----");
    return data;
  }

}