import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/integration/restclient/token_rest_client.dart';

class HeaderBuilder {
  static Map<String, String> rawHeader() {
    return {
      "Content-Type": "application/json",
    };
  }

  static Future<Map<String, String>> tokenHeader(BuildContext context) async {
    return {
      "Content-Type": "application/json",
      "Authorization" : await getToken(context),
     };
  }
}
