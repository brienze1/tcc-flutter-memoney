import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/user_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseConfiguration {
  static final String dbName = "memoney.db";

  static Future<Database> getDatabase() async {
    final String path = join(await getDatabasesPath(), dbName);
    return openDatabase(
      path,
      onCreate: (db, version) {
        _createDb(db, version);
      },
      version: 1,
      onDowngrade: onDatabaseDowngradeDelete,
    );
  }

  static void _createDb(Database db, int newVersion) async {
    Batch batch = db.batch();
    batch.execute(ActiveUserDao.activeUserTableSQL);
    batch.execute(UserDao.userTableSQL);
    await batch.commit();
  }

}

