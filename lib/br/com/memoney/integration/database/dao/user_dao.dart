import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/active_user.dart';
import 'package:memoney/br/com/memoney/domain/entity/user.dart';
import 'package:memoney/br/com/memoney/integration/database/configuration/database_configuration.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:sqflite/sqflite.dart';

class UserDao {
  static final String table = "users";
  static final String id = "id";
  static final String firstName = "first_name";
  static final String lastName = "last_name";
  static final String email = "email";
  static final String secret = "secret";
  static final String deviceId = "device_id";
  static final String autoLogin = "auto_login";
  static final String validEmail = "valid_email";

  static final String userTableSQL = "CREATE TABLE $table("
      "$id INTEGER PRIMARY KEY, "
      "$firstName TEXT, "
      "$lastName TEXT, "
      "$email TEXT, "
      "$secret TEXT, "
      "$deviceId TEXT, "
      "$autoLogin BOOLEAN, "
      "$validEmail BOOLEAN)";

  static Future<User> findMainUser() async {
    final Database db = await DatabaseConfiguration.getDatabase();
    List<Map<String, dynamic>> usersMap = await db.query(table);
    List<User> usersList = _getUsers(usersMap);
    for(User user in usersList){
      if(user.autoLogin){
        return user;
      }
    }
    return null;
  }

  static Future<User> setMainUser(User user) async {
    final Database db = await DatabaseConfiguration.getDatabase();
    List<Map<String, dynamic>> usersMap = await db.query(table);
    List<User> usersList = _getUsers(usersMap);
    for(User userFound in usersList){
      if(userFound.autoLogin){
        userFound.autoLogin = false;
        await update(userFound);
      }
    }
    user.autoLogin = true;
    await update(user);
    return user;
  }

  static Future<int> save(User user) async {
    final Database db = await DatabaseConfiguration.getDatabase();
    return db.insert(table, user.toMap());
  }

  static Future<int> update(User user) async {
    final Database db = await DatabaseConfiguration.getDatabase();
    return db.update(table, user.toMap());
  }

  static Future<User> findByEmail(String email) async {
    final Database db = await DatabaseConfiguration.getDatabase();
    List<Map<String, dynamic>> listaMapas = await db.query(table + " WHERE ${UserDao.email}='$email'");
    List<User> users = _getUsers(listaMapas);
    return users.isNotEmpty ? users.first : null;
  }

  static List<User> _getUsers(List<Map<String, dynamic>> listaMapas) {
    final List<User> users = List();
    for (Map<String, dynamic> map in listaMapas) {
      users.add(User.toUser(map));
    }
    return users;
  }

  static Future<User> findActiveUser() async {
    ActiveUser activeUser = await ActiveUserDao.findActiveUser();
    if(activeUser != null){
      return await findByEmail(activeUser.email);
    }
    return null;
  }

}
