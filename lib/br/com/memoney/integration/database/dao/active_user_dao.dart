import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/domain/entity/active_user.dart';
import 'package:memoney/br/com/memoney/integration/database/configuration/database_configuration.dart';
import 'package:sqflite/sqflite.dart';

class ActiveUserDao {
  static final String table = "active_user";
  static final String id = "id";
  static final String email = "email";
  static final String firstName = "first_name";
  static final String lastName = "last_name";
  static final String password = "password";
  static final String secret = "secret";
  static final String deviceId = "device_id";
  static final String token = "token";

  static final String activeUserTableSQL = "CREATE TABLE $table("
      "$id INTEGER PRIMARY KEY, "
      "$email TEXT, "
      "$firstName TEXT, "
      "$lastName TEXT, "
      "$password TEXT, "
      "$secret TEXT, "
      "$deviceId TEXT, "
      "$token TEXT)";

  static Future<int> save(ActiveUser activeUser) async {
    await clearActiveUser();
    final Database db = await DatabaseConfiguration.getDatabase();
    return db.insert(table, activeUser.toMap());
  }

  static Future<ActiveUser> findActiveUser() async {
    final Database db = await DatabaseConfiguration.getDatabase();
    List<Map<String, dynamic>> activeUsers = await db.query(table);
    List<ActiveUser> activeUser = _getActiveUsers(activeUsers);
    if(activeUser.isNotEmpty){
      return activeUser.first;
    }
    debugPrint("No Active user");
    return null;
  }

  static Future<bool> clearActiveUser() async {
    ActiveUser activeUser = await findActiveUser();
    if(activeUser != null) {
      final Database db = await DatabaseConfiguration.getDatabase();
      db.delete(table);
    }
    return true;
  }

  static List<ActiveUser> _getActiveUsers(List<Map<String, dynamic>> listaMapas) {
    final List<ActiveUser> activeUsers = List();
    for (Map<String, dynamic> map in listaMapas) {
      activeUsers.add(ActiveUser.toActiveUSer(map));
    }
    return activeUsers;
  }
}