import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:memoney/br/com/memoney/domain/entity/active_user.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';
import 'package:uuid/uuid.dart';

Future<Map<dynamic, dynamic>> decode(String token, String secret) async {
  JwtClaim jwt;
  try{
    if (secret != null && secret.trim().isNotEmpty) {
      jwt = verifyJwtHS256Signature(token, secret);
    } else {
      ActiveUser activeUser = await ActiveUserDao.findActiveUser();
      jwt = verifyJwtHS256Signature(token, activeUser.secret);
    }
  } catch (e) {
    debugPrint(e.toString());
    throw e;
  }

  return  jwt.toJson();
}

Future<String> encode(String json, String secret) async {
  ActiveUser activeUser = await ActiveUserDao.findActiveUser();
  final claimSet = new JwtClaim(
      issuer: "brienze_api_dart",
      subject: activeUser != null ? activeUser.id.toString() : null,
      jwtId: Uuid().v1(),
      payload: jsonDecode(json),
      expiry: DateTimeBrazilia.now().add(Duration(minutes: 5)));

  if (secret != null && secret.trim().isNotEmpty) {
    return issueJwtHS256(claimSet, secret);
  } else {
    return issueJwtHS256(claimSet, activeUser.secret);
  }
}

Future<bool> validate(String token, String secret) async {
  JwtClaim jwt;
  try{
    if (secret != null && secret.trim().isNotEmpty) {
      jwt = verifyJwtHS256Signature(token, secret);
    } else {
      ActiveUser activeUser = await ActiveUserDao.findActiveUser();
      jwt = verifyJwtHS256Signature(token, activeUser.secret);
    }
  } catch (e) {
    debugPrint(e.toString());
    return false;
  }

  return jwt != null;
}

Future<DateTime> getExpiryDate(String token, String secret) async {
  JwtClaim jwt;
  try{
    if (secret != null && secret.trim().isNotEmpty) {
      jwt = verifyJwtHS256Signature(token, secret);
    } else {
      ActiveUser activeUser = await ActiveUserDao.findActiveUser();
      jwt = verifyJwtHS256Signature(token, activeUser.secret);
    }
  } catch (e) {
    debugPrint(e.toString());
    throw e;
  }

  return jwt.expiry.subtract(Duration(hours: 3));
}

Map decodeWithoutVerification(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

String _decodeBase64(String splittedToken) {
  String normalizedSource = base64Url.normalize(splittedToken);
  return utf8.decode(base64Url.decode(normalizedSource));
}