import 'dart:convert';

import 'package:memoney/br/com/memoney/domain/entity/device.dart';
import 'package:memoney/br/com/memoney/domain/entity/user.dart';

class UserDto {
  final String firstName;
  final String lastName;
  final String email;
  final String password;
  final String secret;
  final Device device;
  final bool autoLogin;
  final bool validEmail;

  UserDto({this.firstName, this.lastName, this.email, this.password, this.device, this.autoLogin, this.secret, this.validEmail});

  String toJson() {
    Map<String, dynamic> mapa = new Map();
    mapa["first_name"] = this.firstName;
    mapa["last_name"] = this.lastName;
    mapa["email"] = this.email;
    mapa["password"] = this.password;
    mapa["device"] = this.device.toMap();
    mapa["auto_login"] = this.autoLogin;
    mapa["secret"] = this.secret;
    return jsonEncode(mapa);
  }

  static UserDto toUserDto(Map<String, dynamic> userDtoMap) {
    return UserDto(
        firstName: userDtoMap["first_name"],
        lastName: userDtoMap["last_name"],
        email: userDtoMap["email"],
        password: userDtoMap["password"],
        device: Device.toDevice(userDtoMap["device"]),
        autoLogin: userDtoMap["auto_login"],
        validEmail: userDtoMap["valid_email"],
        secret: userDtoMap["secret"]);
  }

  User toUser() {
    return User(
        firstName: this.firstName,
        lastName: this.lastName,
        secret: this.secret,
        deviceId: this.device.deviceId,
        autoLogin: this.autoLogin,
        validEmail: this.validEmail,
        email: this.email);
  }

  @override
  String toString() {
    return 'UserDto{firstName: $firstName, lastName: $lastName, email: $email, password: $password, secret: $secret, device: $device, autoLogin: $autoLogin, validEmail: $validEmail}';
  }
}
