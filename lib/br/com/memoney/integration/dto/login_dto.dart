import 'dart:convert';

class LoginDto {
  final String email;
  final String password;
  final String deviceId;

  LoginDto({this.email, this.password, this.deviceId});

  String toJson(){
    Map<String, dynamic> mapa = new Map();
    mapa["email"] = this.email;
    mapa["password"] = this.password;
    mapa["device_id"] = this.deviceId;
    return jsonEncode(mapa);
  }
}