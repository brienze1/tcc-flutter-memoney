import 'package:url_launcher/url_launcher.dart';

Future<bool> launchURL(String urlString) async {
  if(await canLaunch(urlString)){
    await launch(urlString);
    return true;
  } else {
    return false;
  }
}