import 'package:http/http.dart';
import 'package:http_interceptor/http_client_with_interceptor.dart';
import 'package:memoney/br/com/memoney/integration/builder/logging_interceptor_builder.dart';

class GenericRestClient {
  static Future<Response> post(String url,
      {String body,
      Map<String, String> headers,
      String path,
      Map<String, String> query}) async {
    String newUrl = buildUrl(url, path, query);
    return await _getClient()
        .post(
          newUrl,
          headers: headers,
          body: body,
        )
        .timeout(Duration(seconds: 5));
  }

  static get(String url,
      {Map<String, String> headers,
      String path,
      Map<String, String> query}) async {
    String newUrl = buildUrl(url, path, query);
    return await _getClient()
        .get(
          newUrl,
          headers: headers,
        )
        .timeout(Duration(seconds: 5));
  }

  static String buildUrl(String url, String path, Map<String, String> query) {
    String newUrl = url;

    if (path != null) {
      if (path.startsWith("/")) {
        newUrl = newUrl + path;
      } else {
        newUrl = newUrl + "/" + path;
      }
    }
    if (query != null && query.isNotEmpty) {
      for (int i = 0; i < query.length; i++) {
        if (i == 0) {
          newUrl = newUrl +
              "?" +
              query.entries.elementAt(i).key +
              "=" +
              query.entries.elementAt(i).value;
        } else {
          newUrl = newUrl +
              "&" +
              query.entries.elementAt(i).key +
              "=" +
              query.entries.elementAt(i).value;
        }
      }
    }

    return newUrl;
  }

  static Client _getClient() {
    return HttpClientWithInterceptor.build(
      interceptors: [LoggingInterceptorBuilder()],
    );
  }
}
