import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/config/application_config.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/integration/builder/header_builder.dart';

import 'generic_rest_client.dart';

Future<List<Transaction>> getTransactions(
    BuildContext context, Map<String, String> query) async {
  final String path = "/v1/transactions/find";

  Response response = await GenericRestClient.get(
    AppConfig.of(context).apiBaseUrl,
    headers: await HeaderBuilder.tokenHeader(context),
    path: path,
    query: query,
  );
  Iterable jsonList = jsonDecode(response.body);
  List<Transaction> transactions = [];
  for (Map<String, dynamic> json in jsonList) {
    transactions.add(Transaction.fromJson(json));
  }

  return transactions;
}

Future<List<Transaction>> getLastTransactions(BuildContext context,
    {int quantity, DateTime date, int page}) async {
  Map<String, String> query = Map();
  if (quantity != null && quantity > 0) {
    query["quantity"] = quantity.toString();
  }
  if (page != null && page > 0) {
    query["page"] = page.toString();
  }
  if (date != null) {
    query["since"] = DateFormat("dd/MM/yyyy").format(date);
  }

  return await getTransactions(context, query);
}

Future<Transaction> postNewTransaction(
    BuildContext context, Transaction transaction) async {
  final String path = "/v1/transactions/register";

  Response response = await GenericRestClient.post(
    AppConfig.of(context).apiBaseUrl,
    body: transaction.toJson(),
    headers: await HeaderBuilder.tokenHeader(context),
    path: path
  );

  Map<String, dynamic> json = jsonDecode(response.body);
  return Transaction.fromJson(json);
}
