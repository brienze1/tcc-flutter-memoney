import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:memoney/br/com/memoney/config/application_config.dart';
import 'package:memoney/br/com/memoney/integration/builder/header_builder.dart';
import 'package:memoney/br/com/memoney/integration/dto/user_dto.dart';
import 'package:memoney/br/com/memoney/integration/restclient/generic_rest_client.dart';

Future<String> newUserRestClient(BuildContext context, UserDto userDto) async {
  final String registrationURL = "/v1/user/new";

  Response response = await GenericRestClient.post(AppConfig.of(context).apiBaseUrl + registrationURL, headers: HeaderBuilder.rawHeader(), body: userDto.toJson());
  Map<String, dynamic> json = jsonDecode(response.body);

  String registrationToken = json["data"];

  if(registrationToken != null) {
    return registrationToken;
  }
  throw new Exception("Erro ao registrar");
}
