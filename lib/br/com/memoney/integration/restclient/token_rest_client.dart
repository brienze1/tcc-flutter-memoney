import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:memoney/br/com/memoney/config/application_config.dart';
import 'package:memoney/br/com/memoney/domain/entity/active_user.dart';
import 'package:memoney/br/com/memoney/integration/builder/header_builder.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:memoney/br/com/memoney/integration/dto/login_dto.dart';
import 'package:memoney/br/com/memoney/integration/restclient/generic_rest_client.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';
import 'package:memoney/br/com/memoney/integration/utils/jwt_converter.dart';

Future<String> getToken(BuildContext context) async {
  final String acessToken = "access_token";
  final String tokenURL = "/v1/user/login";

  ActiveUser activeUser = await ActiveUserDao.findActiveUser();
  if (activeUser == null) {
    throw new Exception("No Active User");
  }

  bool isValidToken = await _isValidToken(activeUser.token);
  if (isValidToken) {
    return activeUser.token;
  }

  LoginDto loginDto = LoginDto(email: activeUser.email.toLowerCase(), password: activeUser.password, deviceId: activeUser.deviceId);
  Response response = await GenericRestClient.post(AppConfig.of(context).apiBaseUrl + tokenURL,
      headers: HeaderBuilder.rawHeader(), body: loginDto.toJson());
  Map<String, dynamic> json = jsonDecode(response.body);
  String token = json[acessToken];

  ActiveUserDao.save(ActiveUser(
    email: activeUser.email.toLowerCase(),
    firstName: activeUser.firstName,
    lastName: activeUser.lastName,
    password: activeUser.password,
    token: token,
    deviceId: activeUser.deviceId,
    secret: activeUser.secret,
  ));
  return token;
}

Future<bool> _isValidToken(String token) async {
  try {
    if (token != null) {
      ActiveUser activeUser = await ActiveUserDao.findActiveUser();

      bool isValidToken = await validate(token, activeUser.secret);
      if (!isValidToken) {
        return false;
      }

      DateTime exp = await getExpiryDate(token, activeUser.secret);

      return exp.isAfter(
          DateTimeBrazilia.now().subtract(Duration(seconds: 20)));
    }
  }catch (e) {
    debugPrint(e.toString());
    return false;
  }
  return false;
}
