import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/builder/screen_builder.dart';
import 'package:memoney/br/com/memoney/delivery/components/back_button.dart';
import 'package:memoney/br/com/memoney/delivery/components/button.dart';
import 'package:memoney/br/com/memoney/delivery/components/shake_widget.dart';
import 'package:memoney/br/com/memoney/delivery/components/text_input_form.dart';
import 'package:memoney/br/com/memoney/delivery/screens/unknown_error.dart';
import 'package:memoney/br/com/memoney/domain/rules/registration_rules.dart';
import 'package:memoney/br/com/memoney/domain/service/recover_password_service.dart';

import 'check_your_email.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  Color _recoverButtonColor;
  Color _emailUnderlineColor;
  String _email = "";
  UniqueKey _emailShakeKey = UniqueKey();
  int _shakeDuration = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 32, right: 32, top: 60, bottom: 44),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomBackButton(),
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Text(
                    "Forgot Password",
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Text(
                    "Please enter your registered email or mobile to reset your password",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                ShakeWidget(
                  key: _emailShakeKey,
                  duration: Duration(milliseconds: _shakeDuration),
                  child: TextInputForm(
                    padding: const EdgeInsets.only(top: 45, bottom: 0),
                    title: "Email",
                    underlineColor: _emailUnderlineColor,
                    onChanged: (text) {
                      updateRecoverButtonColor(email: text);
                    },
                  ),
                ),
                Button(
                  padding: const EdgeInsets.only(top: 55),
                  title: "Recover Password",
                  color: _recoverButtonColor,
                  onTap: () {
                    if (validateEmail(_email)) {
                      Navigator.push(context, MaterialPageRoute(builder: (context) {
                        return ScreenBuilder(
                          future: recoverPassword(_email),
                          child: CheckYourEmail(),
                          fallBackChild: UnknownError(),
                        );
                      }));
                    } else {
                      shakeEmptyFields();
                      setState(() {});
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  updateRecoverButtonColor({String email}) {
    if (email != null) {
      _email = email;
      if (validateEmail(_email)) {
        _emailUnderlineColor = Theme.of(context).primaryColor;
        _recoverButtonColor = Theme.of(context).primaryColor;
      } else {
        _emailUnderlineColor = Theme.of(context).accentColor;
        _recoverButtonColor = Theme.of(context).accentColor;
      }
    }

    _updateState();
  }

  void shakeEmptyFields() {
    _shakeDuration = 650;
    if (!validateEmail(_email)) {
      _emailShakeKey = new UniqueKey();
      _emailUnderlineColor = Theme.of(context).highlightColor;
    }
    _updateState();
  }

  void _updateState() {
    setState(() {});
  }
}
