import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/components/basic_scaffold.dart';

class Regards extends StatelessWidget {
  final ScrollController _scrollController = new ScrollController();

  static final String repoURL = "https://gitlab.com/brienze1/tcc-memoney";

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      regardsScreen: true,
      showBottomBar: false,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Colors.white, width: 0),
            ),
            color: Theme.of(context).primaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32, top: 46, bottom: 24),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Regards",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(left: 32, right: 32, top: 1, bottom: 40),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(24), topLeft: Radius.circular(24)),
                    color: Colors.white,
                  ),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                padding: const EdgeInsets.only(top: 30),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        "Acknowledgement\n",
                                        style: Theme.of(context).textTheme.overline
                                    ),
                                    Text(
                                      "It has been a great opportunity to gain lots of \n"
                                      "experience in real time projects, followed by the \n"
                                      "knowledge of how to actually design and analyze real\n"
                                      "projects. \n\n"
                                      "For that I want to thank all the people \n"
                                      "who made it possible. \n\n"
                                      "Special thanks to the graduation teacher advisor \n"
                                      "Dr. Marcio Alexandre Marques for the efforts he did \n"
                                      "to provide me with all useful information and making \n"
                                      "the path clear to implement all the project design \n"
                                      "and analysis. \n\n"
                                      "Furthermore, I want to thank all the professors of \n"
                                      "UNESP for the interesting lectures they presented \n"
                                      "along my graduation which had great benefit for all \n"
                                      "the students. \n\n"
                                      "In addition, I would like to express \n"
                                      "my sincere appreciations to our Fraternity Republica \n"
                                      "Hentrometeu for their guidance, continuous \n"
                                      "encouragement and support during the course.  \n\n"
                                      "At last, I would like to thank all the people who \n"
                                      "helped, supported and encouraged me to successfully \n"
                                      "finish the graduation project whether they were in \n"
                                      "the university or not. \n\n"
                                      "Furthermore, I have to show my gratitude to Eng. \n"
                                      "Beatriz Stadler for her help in some parts related \n"
                                      "to the design work.",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
