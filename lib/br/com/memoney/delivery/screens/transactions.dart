import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/components/basic_scaffold.dart';
import 'package:memoney/br/com/memoney/delivery/components/transaction_card.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/domain/service/transaction_service.dart';

class Transactions extends StatefulWidget {
  @override
  _TransactionsState createState() => _TransactionsState();
}

class _TransactionsState extends State<Transactions> {
  List<Transaction> _transactions = List();
  ScrollController _scrollController = new ScrollController();
  int _transactionPage = 0;
  bool _noTransactionsRegistered = false;
  bool _noLeftTransactions = false;
  bool _loadingNewData = false;

  @override
  void initState() {
    super.initState();
    _init();
  }

  void _init() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
          _getLatestTransactions();
          _scrollController.addListener(_loadingNewData
              ? () {}
              : () {
                  if (_scrollController.position.pixels ==
                          _scrollController.position.maxScrollExtent &&
                      !_loadingNewData) {
                    if (!_noTransactionsRegistered) {
                      if (_transactions.isNotEmpty) {
                        _loadingNewData = true;
                        setState(() {});
                      }
                      _getLatestTransactions().then((value) {
                        _loadingNewData = false;
                      });
                    }
                  }
                });
        }));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      transactionsScreen: true,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Colors.white, width: 0),
            ),
            color: Theme.of(context).primaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                    left: 32, right: 32, top: 46, bottom: 24),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Transactions",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(
                      left: 32, right: 32, top: 1, bottom: 40),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(24),
                        topLeft: Radius.circular(24)),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      _noTransactionsRegistered
                          ? Padding(
                              padding: const EdgeInsets.only(top: 24),
                              child: FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                        padding: const EdgeInsets.only(top: 30),
                                        child: Text(
                                          "Looks like you don't have any transactions :(",
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .accentColor),
                                        )),
                                  ],
                                ),
                              ),
                            )
                          : _transactions.isEmpty
                              ? Padding(
                                  padding: const EdgeInsets.all(60.0),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Theme.of(context).primaryColor),
                                    ),
                                  ),
                                )
                              : ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: _transactions.length,
                                  itemBuilder: (context, index) {
                                    final transaction = _transactions[index];
                                    return TransactionCard(
                                      transaction: transaction,
                                    );
                                  },
                                ),
                      Visibility(
                        visible:
                            _noLeftTransactions && _transactions.isNotEmpty,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Text(
                              "No transactions left",
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                      _loadingNewData
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Theme.of(context).primaryColor),
                                ),
                              ),
                            )
                          : Container(height: 20)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _getLatestTransactions() async {
    try {
      setState(() {});
      List<Transaction> transactions = await getLatestTransactions(context,
          quantity: 10, page: _transactionPage);
      if (transactions.isEmpty && _transactions.isEmpty) {
        _noTransactionsRegistered = true;
      }
      _noLeftTransactions = true;
      for (Transaction newTransaction in transactions) {
        bool mustAdd = true;
        for (Transaction transaction in _transactions) {
          if (newTransaction.id == transaction.id) {
            mustAdd = false;
          }
        }
        if (mustAdd) {
          _noLeftTransactions = false;
          _transactions.add(newTransaction);
        }
      }

      if (!_noLeftTransactions) {
        _transactionPage++;
      }
      return _transactions;
    } catch (e) {
      debugPrint(e.toString());
      if (_transactions.isEmpty) {
        _noTransactionsRegistered = true;
      }
    } finally {
      _scrollController.animateTo(
          _scrollController.position.maxScrollExtent - 60,
          duration: Duration(milliseconds: 300),
          curve: Curves.ease);
      setState(() {});
      _loadingNewData = false;
      setState(() {});
    }
  }
}
