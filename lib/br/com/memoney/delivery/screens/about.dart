import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/components/basic_scaffold.dart';
import 'package:memoney/br/com/memoney/delivery/components/transparent_button.dart';
import 'package:memoney/br/com/memoney/integration/launch/launch_url.dart';

class About extends StatelessWidget {
  final ScrollController _scrollController = new ScrollController();

  static final String repoURL = "https://gitlab.com/brienze1/tcc-memoney";

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      aboutScreen: true,
      showBottomBar: false,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Colors.white, width: 0),
            ),
            color: Theme.of(context).primaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32, top: 46, bottom: 24),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "About",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(left: 32, right: 32, top: 1, bottom: 40),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(24), topLeft: Radius.circular(24)),
                    color: Colors.white,
                  ),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                padding: const EdgeInsets.only(top: 30),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "The App\n",
                                      style: Theme.of(context).textTheme.overline
                                    ),
                                    Text(
                                      "MeMoney is an app designed to store your\n"
                                      "monetary transactions and help you realize\n"
                                      "where your money goes to. \n",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    Text(
                                      "Features\n",
                                      style: Theme.of(context).textTheme.overline
                                    ),
                                    Text(
                                      "At the begining the features implemented will \n"
                                      "be focussed in basic functions and will increase \n"
                                      "it's functionalities as time goes by. So I'll be \n"
                                      "adding more in the near future. \n\n"

                                      "You may also suggest changes by forking this repo \n"
                                      "and creating a pull request or opening an issue:",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    TransparentButton(
                                      title: About.repoURL + "\n",
                                      onTap: () {
                                        launchURL(About.repoURL);
                                      },
                                      textStyle: TextStyle(color: Colors.pinkAccent),
                                    ),
                                    Text(
                                      "Some implemented features:\n\n"

                                      "   - Account registration\n"
                                      "   - Account login\n"
                                      "   - Store transactions\n"
                                      "   - Visualize transaction graphs\n\n"

                                      "Soon to be added features:\n\n"

                                      "   - Auto login (when configured in app)\n"
                                      "   - Profile page\n"
                                      "   - Config page\n"
                                      "   - Transaction details\n"
                                      "   - Automatic location finder (when configured)\n",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    Text(
                                      "Built With\n",
                                      style: Theme.of(context).textTheme.overline
                                    ),
                                    Text(
                                      "For the backend we used java along with spring-boot\n"
                                      "to build the core of the application.\n"
                                      "The api runs in AWS cloud, we used ECS to run our\n"
                                      "instances and the database we used was DynamoDb\n"
                                      "along with RDS.\n\n"

                                      "The is also a lambda that works sending the \n"
                                      "verification emails.\n\n"

                                      "In the frontend we opted to use Flutter with Dart\n"
                                      "so it would help by build android and ios versions\n"
                                      "faster.\n",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    Text(
                                      "Versions\n",
                                      style: Theme.of(context).textTheme.overline
                                    ),
                                    Text(
                                      "Release-1.0.0:\n"
                                      "First release with core features listed above.",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
