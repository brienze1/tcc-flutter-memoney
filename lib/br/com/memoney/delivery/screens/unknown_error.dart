import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:memoney/br/com/memoney/delivery/components/close_button.dart';

class UnknownError extends StatelessWidget {
  final bool canBeClosed;
  final Color color;
  final Color messageColor;

  UnknownError({this.canBeClosed: true, this.color, this.messageColor});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32, top: 40),
      child: Container(
        color: color != null ? color : null,
        child: Column(
          children: [
            Visibility(
              visible: canBeClosed,
              child: CustomCloseButton(),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.warning,
                    size: 100,
                    color: messageColor != null ? messageColor : null,
                  ),
                  Text(
                    "Unknown Error",
                    style: GoogleFonts.nunitoSans(
                        fontSize: 32,
                        fontWeight: FontWeight.w700,
                        color: messageColor != null ? messageColor : Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
