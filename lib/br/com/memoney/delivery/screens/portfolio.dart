import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/delivery/components/basic_scaffold.dart';
import 'package:memoney/br/com/memoney/delivery/components/custom_toggle_button.dart';
import 'package:memoney/br/com/memoney/delivery/components/transaction_card.dart';
import 'package:memoney/br/com/memoney/delivery/components/transparent_button.dart';
import 'package:memoney/br/com/memoney/delivery/screens/transactions.dart';
import 'package:memoney/br/com/memoney/delivery/screens/unknown_error.dart';
import 'package:memoney/br/com/memoney/domain/builder/datasource_builder.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/domain/service/transaction_service.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class Portfolio extends StatefulWidget {
  @override
  _PortfolioState createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {
  final _formatter = new NumberFormat("#,##0.00", "pt_BR");
  List<bool> _isSelected = [true, false, false, false, false];
  int _selectedIndex = 0;
  List<Transaction> _datasource = List();
  List<Transaction> _latestTransactions = List();
  bool _loadingTransactions = false;
  bool _errorDatasource = false;
  bool _errorLatestTransactions = false;
  DateFormat _dateFormat = DateFormat("ddd hh tt");
  String _balance = "R\$ 0,00";

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
          _toggleButtonSelect(_selectedIndex);
          setState(() {});
        }));
  }

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      portfolioScreen: true,
      body: SingleChildScrollView(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32, top: 46),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      children: [
                        Center(
                          child: Text(
                            "Portfolio",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Center(
                        child: Text(
                          _balance,
                          style: Theme.of(context).textTheme.headline3,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 0),
                  child: Center(
                    child: Container(
                      height: MediaQuery.of(context).size.height / 2,
                      child: _errorDatasource
                          ? UnknownError(
                              canBeClosed: false,
                              color: Theme.of(context).primaryColor,
                              messageColor: Colors.white,
                            )
                          : _datasource.isEmpty
                              ? Container(
                                  height: MediaQuery.of(context).size.height / 2,
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                                    ),
                                  ),
                                )
                              : SfCartesianChart(
                                  plotAreaBorderWidth: 0,
                                  margin: const EdgeInsets.only(bottom: 16),
                                  borderWidth: 0,
                                  primaryXAxis: CategoryAxis(
                                    isVisible: false,
                                    minimum: 1,
                                    maximum: double.tryParse((_datasource.length - 2).toString()),
                                  ),
                                  primaryYAxis: NumericAxis(
                                    isVisible: false,
                                    minimum: _getMin(),
                                    maximum: _getMax(),
                                  ),
                                  tooltipBehavior: TooltipBehavior(
                                      color: Colors.pink[100],
                                      enable: true,
                                      builder: (dynamic data, dynamic point, dynamic series, int pointIndex,
                                          int seriesIndex) {
                                        return Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0x2FFFFFFF),
                                              borderRadius: BorderRadius.all(Radius.circular(200)),
                                            ),
                                            child: FittedBox(
                                              fit: BoxFit.fitHeight,
                                              child: Padding(
                                                padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Text(
                                                      _valor2(_datasource[pointIndex].valor, _datasource[pointIndex].tipo),
                                                      style: Theme.of(context).textTheme.subtitle1,
                                                    ),
                                                    Text("${_dateFormat.format(_datasource[pointIndex].data)}",
                                                        style: Theme.of(context).textTheme.subtitle2),
                                                  ],
                                                ),
                                              ),
                                            ));
                                      }),
                                  plotAreaBorderColor: Colors.white,
                                  title: ChartTitle(
                                      text: "Wallet Balance", textStyle: Theme.of(context).textTheme.headline5),
                                  series: <ChartSeries<Transaction, DateTime>>[
                                    SplineSeries<Transaction, DateTime>(
                                      dataSource: _datasource,
                                      xValueMapper: (Transaction transaction, _) => transaction.data,
                                      yValueMapper: (Transaction transaction, _) => transaction.valor,
                                      width: 2,
                                      color: Colors.white,
                                    )
                                  ],
                                ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 60,
                width: double.infinity,
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
                child: CustomToggleButton(
                  children: [
                    Text("Day", style: Theme.of(context).textTheme.headline5),
                    Text("Week", style: Theme.of(context).textTheme.headline5),
                    Text("Month", style: Theme.of(context).textTheme.headline5),
                    Text("Year", style: Theme.of(context).textTheme.headline5),
                    Text("All", style: Theme.of(context).textTheme.headline5),
                  ],
                  paddingButton: const EdgeInsets.only(left: 10, right: 10, top: 6, bottom: 6),
                  paddingBetweenButtons: const EdgeInsets.only(left: 6, right: 6),
                  buttonBorderRadius: BorderRadius.all(Radius.circular(7)),
                  selectedColor: Colors.pinkAccent[100],
                  onTap: (int index) {
                    setState(() {
                      _isSelected[index] = true;
                      for (int i = 0; i < _isSelected.length; i++) {
                        if (i != index && _isSelected[i]) {
                          _isSelected[i] = false;
                        }
                      }
                      _selectedIndex = index;
                      _datasource = List();
                      setState(() {});
                      _toggleButtonSelect(_selectedIndex);
                    });
                  },
                  isSelected: _isSelected,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32, top: 16),
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(24), topLeft: Radius.circular(24)),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.transparent,
                      child: Text(
                        "Latest transactions",
                        style: GoogleFonts.nunitoSans(fontSize: 16, color: Colors.black, fontWeight: FontWeight.w700),
                      ),
                    ),
                    _errorLatestTransactions
                        ? UnknownError(
                            color: Theme.of(context).backgroundColor,
                            canBeClosed: false,
                            messageColor: Theme.of(context).primaryColor,
                          )
                        : _loadingTransactions
                            ? Padding(
                                padding: const EdgeInsets.all(60.0),
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor: new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                                  ),
                                ),
                              )
                            : _latestTransactions.isEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 24),
                                    child: Center(
                                      child: Text(
                                        "Looks like you don't have any transactions :("
                                      ),
                                    ),
                                  )
                                : ListView.builder(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: _latestTransactions.length,
                                    itemBuilder: (context, index) {
                                      final transaction = _latestTransactions[index];
                                      return TransactionCard(transaction: transaction,);
                                    },
                                  ),
                    Center(
                      child: Container(
                        padding: const EdgeInsets.only(top: 30, bottom: 40),
                        child: TransparentButton(
                          title: "See All Transactions",
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return Transactions();
                            }));
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  double _getMin() {
    double max = 0;
    double min = 0;
    for (Transaction transaction in _datasource) {
      if(transaction.valor < min) {
        min = transaction.valor;
      }
      if(transaction.valor > max) {
        max = transaction.valor;
      }
    }
    if (min > 0) {
      return -min;
    } else if (min < 0) {
      return 2*min;
    } else if(max < 0) {
      return 2*max;
    } else if(max > 0) {
      return -max;
    } else {
      return -10;
    }
  }

  double _getMax() {
    double max = 0;
    double min = 0;
    for (Transaction transaction in _datasource) {
      if(transaction.valor > max) {
        max = transaction.valor;
      }
      if(transaction.valor < min) {
        min = transaction.valor;
      }
    }
    if (max > 0) {
      return 2*max;
    } else if (max < 0) {
      return -max;
    } else if(min < 0) {
      return -min;
    } else if(min > 0) {
      return 2*min;
    } else {
      return 10;
    }
  }

  _toggleButtonSelect(int index) async {
    try {
      _loadingTransactions = true;
      _latestTransactions = await getLatestTransactions(context, quantity: 5);
      _loadingTransactions = false;
    } catch (e) {
      _errorLatestTransactions = true;
    }
    try {
      List<Transaction> transactions;
      switch (index) {
        case 0:
          transactions = buildTransactionDataSource(await getLatestTransactions(context, daysAgo: 0), "day");
          break;
        case 1:
          transactions = buildTransactionDataSource(await getLatestTransactions(context, daysAgo: 6), "week");
          break;
        case 2:
          transactions = buildTransactionDataSource(await getLatestTransactions(context, daysAgo: 30), "month");
          break;
        case 3:
          transactions = buildTransactionDataSource(await getLatestTransactions(context, daysAgo: 365), "year");
          break;
        case 4:
          transactions = buildTransactionDataSource(await getLatestTransactions(context), "all");
          break;
      }

      if (transactions.first.data.isAfter(DateTimeBrazilia.now().subtract(Duration(days: 2)))) {
        _dateFormat = DateFormat("EEEE hh a");
      } else if (transactions.first.data.isAfter(DateTimeBrazilia.now().subtract(Duration(days: 33)))) {
        _dateFormat = DateFormat("EEE, MMM d");
      } else {
        _dateFormat = DateFormat("yMMMM");
      }

      _datasource = transactions;

      double value = 0;
      for (Transaction transaction in _datasource) {
        value += transaction.valor;
      }

      _balance = _valor(value);

      return;
    } catch (e) {
      debugPrint(e.toString());
      _errorDatasource = true;
    } finally {
      setState(() {});
    }
  }

  String _valor(double value) {
    String newText = "R\$ " + _formatter.format(value);
    return newText;
  }

  String _valor2(double value, String type) {
    String newText = "R\$ " + _formatter.format(value);

    if(type == "Sent") {
      newText = "-" + newText;
    }

    return newText;
  }
}
