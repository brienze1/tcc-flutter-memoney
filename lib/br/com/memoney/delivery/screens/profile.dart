import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/delivery/components/basic_scaffold.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      showBottomBar: false,
      profileScreen: true,
      body: Container(
        child: Text("Profile"),
      ),
    );
  }
}
