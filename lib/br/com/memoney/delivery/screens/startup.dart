import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/domain/service/login_service.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';

import 'loading.dart';
import 'login.dart';
import 'portfolio.dart';

class Startup extends StatefulWidget {
  @override
  _StartupState createState() => _StartupState();
}

class _StartupState extends State<Startup> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
      _startup();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return LoadingScreen();
  }

  _startup() async {
    await ActiveUserDao.clearActiveUser();
    bool _hasAutoLogin = await hasAutoLogin();
    if(_hasAutoLogin){
      bool _autoLogin = await autoLogin();
      if(_autoLogin) {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return Portfolio();
        }));
      }
    }
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return Login();
    }));
  }

}
