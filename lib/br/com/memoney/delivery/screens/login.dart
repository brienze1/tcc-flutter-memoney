import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/components/app_logo.dart';
import 'package:memoney/br/com/memoney/delivery/components/button.dart';
import 'package:memoney/br/com/memoney/delivery/components/shake_widget.dart';
import 'package:memoney/br/com/memoney/delivery/components/text_input_form.dart';
import 'package:memoney/br/com/memoney/delivery/components/transparent_button.dart';
import 'package:memoney/br/com/memoney/delivery/screens/register.dart';
import 'package:memoney/br/com/memoney/delivery/screens/unknown_error.dart';
import 'package:memoney/br/com/memoney/domain/rules/login_rules.dart';
import 'package:memoney/br/com/memoney/domain/rules/registration_rules.dart';
import 'package:memoney/br/com/memoney/domain/service/login_service.dart';
import 'package:memoney/br/com/memoney/domain/service/register_service.dart';

import 'forgot_password.dart';
import 'portfolio.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  Color _loginButtonColor;
  Color _emailUnderlineColor;
  Color _passwordUnderlineColor;
  String _email = "";
  String _password = "";
  UniqueKey _emailShakeKey = UniqueKey();
  UniqueKey _passwordShakeKey = UniqueKey();
  int _shakeDuration = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 32, right: 32, top: 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppLogo(),
              Column(
                children: [
                  ShakeWidget(
                    key: _emailShakeKey,
                    duration: Duration(milliseconds: _shakeDuration),
                    child: TextInputForm(
                      padding: const EdgeInsets.only(top: 24),
                      title: "Email:",
                      controller: _emailController,
                      underlineColor: _emailUnderlineColor,
                      onEditingComplete: () => FocusScope.of(context).nextFocus(),
                      onChanged: (email) {
                        updateLoginButton(email: email);
                      },
                    ),
                  ),
                  ShakeWidget(
                    key: _passwordShakeKey,
                    duration: Duration(milliseconds: _shakeDuration),
                    child: TextInputForm(
                      title: "Password:",
                      hideText: true,
                      controller: _passwordController,
                      underlineColor: _passwordUnderlineColor,
                      onFieldSubmitted: (_) => FocusScope.of(context).unfocus(),
                      onChanged: (password) {
                        updateLoginButton(password: password);
                      },
                    ),
                  ),
                  Container(
                    alignment: Alignment.topRight,
                    child: TransparentButton(
                      title: "Forgot Password?",
                      padding: const EdgeInsets.only(top: 10, bottom: 20),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return ForgotPassword();
                        }));
                      },
                    ),
                  ),
                  Button(
                    title: "Login",
                    padding: const EdgeInsets.only(bottom: 16),
                    onTap: () {
                      if (validateLogin(_email, _password)) {
                        userIsRegisteredToDevice(_email).then((isRegistered) {
                          if (isRegistered) {
                            _login();
                          } else {
                            registerDevice(context, _email, _password).then((isRegistered) {
                              if (isRegistered) {
                                _login();
                              } else {
                                return UnknownError();
                              }
                            });
                          }
                        });
                      } else {
                        shakeEmptyFields();
                      }
                    },
                    color: _loginButtonColor,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Don't have an account? ",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        TransparentButton(
                          title: "Register Now",
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return Register();
                            }));
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _login() async {
    bool validLogin = await login(context, _email.toLowerCase(), _password);
    if (validLogin) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return Portfolio();
      }));
    }
  }

  void updateLoginButton({String email, String password}) {
    if (email != null) {
      _email = email;
      if (validateEmail(_email)) {
        _emailUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _emailUnderlineColor = Theme.of(context).accentColor;
      }
    }
    if (password != null) {
      _password = password;
      if (validatePassword(_password)) {
        _passwordUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _passwordUnderlineColor = Theme.of(context).accentColor;
      }
    }
    if (validateLogin(_email, _password)) {
      _loginButtonColor = Theme.of(context).primaryColor;
    } else {
      _loginButtonColor = Theme.of(context).accentColor;
    }
    updateState();
  }

  void shakeEmptyFields() {
    _shakeDuration = 650;
    if (!validateEmail(_email)) {
      _emailShakeKey = new UniqueKey();
      _emailUnderlineColor = Theme.of(context).highlightColor;
    }
    if (!validatePassword(_password)) {
      _passwordShakeKey = new UniqueKey();
      _passwordUnderlineColor = Theme.of(context).highlightColor;
    }
    updateState();
  }

  void updateState() {
    setState(() {});
  }
}
