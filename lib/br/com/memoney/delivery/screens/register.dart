import 'package:flutter/material.dart';
import 'package:memoney/br/com/memoney/delivery/builder/screen_builder.dart';
import 'package:memoney/br/com/memoney/delivery/components/back_button.dart';
import 'package:memoney/br/com/memoney/delivery/components/button.dart';
import 'package:memoney/br/com/memoney/delivery/components/shake_widget.dart';
import 'package:memoney/br/com/memoney/delivery/components/text_input_form.dart';
import 'package:memoney/br/com/memoney/delivery/components/transparent_button.dart';
import 'package:memoney/br/com/memoney/delivery/screens/unknown_error.dart';
import 'package:memoney/br/com/memoney/domain/rules/registration_rules.dart';
import 'package:memoney/br/com/memoney/domain/service/register_service.dart';
import 'package:memoney/br/com/memoney/integration/launch/launch_url.dart';

import 'check_your_email.dart';

class Register extends StatefulWidget {
  static const double paddingTop = 0.0;
  static const double paddingBottom = 0.0;
  static final String termsConditionsURL = "https://www.websitepolicies.com/policies/view/o7ELGg1I";
  static final String privacyPolicyURL = "https://www.websitepolicies.com/policies/view/2gPZAUPK";

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  Color _registerButtonColor;
  Color _firstNameUnderlineColor;
  Color _lastNameUnderlineColor;
  Color _emailUnderlineColor;
  Color _passwordUnderlineColor;
  Color _confirmPasswordUnderlineColor;
  String _firstName = "";
  String _lastName = "";
  String _email = "";
  String _password = "";
  String _confirmPassword = "";
  int _shakeDuration = 0;
  UniqueKey _firstNameShakeKey = UniqueKey();
  UniqueKey _lastNameShakeKey = UniqueKey();
  UniqueKey _emailShakeKey = UniqueKey();
  UniqueKey _passwordShakeKey = UniqueKey();
  UniqueKey _confirmPasswordShakeKey = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.only(left: 32, right: 32, top: 60, bottom: 44),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomBackButton(),
                    Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: Text(
                        "Register to MeMooney",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                    ShakeWidget(
                      key: _firstNameShakeKey,
                      duration: Duration(milliseconds: _shakeDuration),
                      child: TextInputForm(
                        padding: const EdgeInsets.only(top: 16, bottom: Register.paddingBottom),
                        controller: _nameController,
                        title: "First Name",
                        underlineColor: _firstNameUnderlineColor,
                        onEditingComplete: () => FocusScope.of(context).nextFocus(),
                        onChanged: (text) {
                          updateRegisterButtonColor(firstName: text);
                        },
                      ),
                    ),
                    ShakeWidget(
                      key: _lastNameShakeKey,
                      duration: Duration(milliseconds: _shakeDuration),
                      child: TextInputForm(
                        padding: const EdgeInsets.only(top: Register.paddingTop, bottom: Register.paddingBottom),
                        controller: _lastNameController,
                        title: "Last Name",
                        underlineColor: _lastNameUnderlineColor,
                        onEditingComplete: () => FocusScope.of(context).nextFocus(),
                        onChanged: (text) {
                          updateRegisterButtonColor(lastName: text);
                        },
                      ),
                    ),
                    ShakeWidget(
                      key: _emailShakeKey,
                      duration: Duration(milliseconds: _shakeDuration),
                      child: TextInputForm(
                        padding: const EdgeInsets.only(top: Register.paddingTop, bottom: Register.paddingBottom),
                        controller: _emailController,
                        title: "Email",
                        underlineColor: _emailUnderlineColor,
                        onEditingComplete: () => FocusScope.of(context).nextFocus(),
                        onChanged: (text) {
                          updateRegisterButtonColor(email: text);
                        },
                      ),
                    ),
                    ShakeWidget(
                      key: _passwordShakeKey,
                      duration: Duration(milliseconds: _shakeDuration),
                      child: TextInputForm(
                        padding: const EdgeInsets.only(top: Register.paddingTop, bottom: Register.paddingBottom),
                        controller: _passwordController,
                        title: "Password",
                        underlineColor: _passwordUnderlineColor,
                        onEditingComplete: () => FocusScope.of(context).nextFocus(),
                        hideText: true,
                        onChanged: (text) {
                          updateRegisterButtonColor(password: text);
                        },
                      ),
                    ),
                    ShakeWidget(
                      key: _confirmPasswordShakeKey,
                      duration: Duration(milliseconds: _shakeDuration),
                      child: TextInputForm(
                        padding: const EdgeInsets.only(top: Register.paddingTop, bottom: Register.paddingBottom),
                        controller: _confirmPasswordController,
                        title: "Confirm Password",
                        underlineColor: _confirmPasswordUnderlineColor,
                        onEditingComplete: () => FocusScope.of(context).unfocus(),
                        underlineFocusColor: Colors.pink,
                        hideText: true,
                        onChanged: (text) {
                          updateRegisterButtonColor(confirmPassword: text);
                        },
                      ),
                    ),
                    Button(
                      padding: const EdgeInsets.only(top: 50),
                      title: "Register",
                      color: _registerButtonColor,
                      onTap: () {
                        if (validateRegistration(_firstName, _lastName, _email, _password, _confirmPassword)) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) {
                            return ScreenBuilder(
                              future: register(context, _firstName, _lastName, _email, _password),
                              child: CheckYourEmail(),
                              fallBackChild: UnknownError(),
                            );
                          }));
                        } else {
                          shakeEmptyFields();
                          setState(() {});
                        }
                      },
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "By registering you agree to ",
                                ),
                                TransparentButton(
                                  title: "Terms & Conditions",
                                  onTap: () {
                                    launchURL(Register.termsConditionsURL);
                                  },
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "and ",
                                ),
                                TransparentButton(
                                  title: "Privacy Policy",
                                  onTap: () {
                                    launchURL(Register.privacyPolicyURL);
                                  },
                                ),
                                Text(
                                  " of MeMoney",
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
    );
  }

  updateRegisterButtonColor(
      {String firstName, String lastName, String email, String password, String confirmPassword}) {
    if (firstName != null) {
      _firstName = firstName;
      if (validateName(_firstName)) {
        _firstNameUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _firstNameUnderlineColor = Theme.of(context).accentColor;
      }
    }

    if (lastName != null) {
      _lastName = lastName;
      if (validateName(_lastName)) {
        _lastNameUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _lastNameUnderlineColor = Theme.of(context).accentColor;
      }
    }

    if (email != null) {
      _email = email;
      if (validateEmail(_email)) {
        _emailUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _emailUnderlineColor = Theme.of(context).accentColor;
      }
    }

    if (password != null) {
      _password = password;
      if (validatePassword(_password)) {
        _passwordUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _passwordUnderlineColor = Theme.of(context).accentColor;
      }
    }

    if (confirmPassword != null) {
      _confirmPassword = confirmPassword;
      if (validateConfirmPassword(_password, _confirmPassword)) {
        _confirmPasswordUnderlineColor = Theme.of(context).primaryColor;
      } else {
        _confirmPasswordUnderlineColor = Theme.of(context).accentColor;
      }
    }

    if (validateRegistration(_firstName, _lastName, _email, _password, _confirmPassword)) {
      _registerButtonColor = Theme.of(context).primaryColor;
    } else {
      _registerButtonColor = Theme.of(context).accentColor;
    }

    _updateState();
  }

  void shakeEmptyFields() {
    _shakeDuration = 650;

    if (!validateName(_firstName)) {
      _firstNameShakeKey = new UniqueKey();
      _firstNameUnderlineColor = Theme.of(context).highlightColor;
    }

    if (!validateName(_lastName)) {
      _lastNameShakeKey = new UniqueKey();
      _lastNameUnderlineColor = Theme.of(context).highlightColor;
    }

    if (!validateEmail(_email)) {
      _emailShakeKey = new UniqueKey();
      _emailUnderlineColor = Theme.of(context).highlightColor;
    }

    if (!validatePassword(_password)) {
      _passwordShakeKey = new UniqueKey();
      _passwordUnderlineColor = Theme.of(context).highlightColor;
    }

    if (!validateConfirmPassword(_password, _confirmPassword)) {
      _confirmPasswordShakeKey = new UniqueKey();
      _confirmPasswordUnderlineColor = Theme.of(context).highlightColor;
    }

    _updateState();
  }

  void _updateState() {
    setState(() {});
  }
}
