import 'package:flutter/cupertino.dart';
import 'package:memoney/br/com/memoney/delivery/screens/loading.dart';
import 'package:memoney/br/com/memoney/delivery/screens/unknown_error.dart';

class ScreenBuilder extends StatelessWidget {
  final Widget child;
  final Future<dynamic> future;
  final Widget fallBackChild;
  final Function(dynamic) onFuture;

  ScreenBuilder({this.child, this.future, this.fallBackChild, this.onFuture});

  @override
  Widget build(context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            break;
          case ConnectionState.waiting:
            return LoadingScreen(title: "loading");
          case ConnectionState.active:
            break;
          case ConnectionState.done:
            dynamic data = snapshot.data;
            if (data is bool) {
              if (onFuture != null) {
                onFuture(data);
              } else if (child != null && data != null && data) {
                return child;
              } else {
                if (fallBackChild != null) {
                  return fallBackChild;
                }
                return UnknownError();
              }
            } else {
              return onFuture(data);
            }
        }
        return UnknownError();
      },
    );
  }
}
