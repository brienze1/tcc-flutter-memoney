import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextInputForm extends StatelessWidget {
  final TextEditingController controller;
  final String title;
  final bool hideText;
  final Function(String text) onChanged;
  final EdgeInsetsGeometry padding;
  final Color underlineColor;
  final Color underlineFocusColor;
  final Function onEditingComplete;
  final Function onFieldSubmitted;

  TextInputForm({
    Key key,
    @required this.title,
    this.hideText,
    this.controller,
    this.onChanged,
    this.underlineColor,
    this.underlineFocusColor,
    this.padding,
    this.onEditingComplete,
    this.onFieldSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null ? padding : const EdgeInsets.all(0.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: TextFormField(
              onEditingComplete: onEditingComplete,
              onFieldSubmitted: onFieldSubmitted,
              controller: controller,
              textInputAction: TextInputAction.done,
              cursorHeight: 30,
              cursorColor: Theme.of(context).primaryColor,
              textAlign: TextAlign.start,
              autofocus: false,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 15),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: underlineColor != null ? underlineColor : Theme.of(context).accentColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: underlineFocusColor != null ? underlineFocusColor : Theme.of(context).primaryColor),
                ),
                labelText: title,
                labelStyle: Theme.of(context).textTheme.caption,
              ),
              style: Theme.of(context).textTheme.bodyText1,

              onChanged: onChanged != null ? onChanged : null,
              obscureText: hideText == null ? false : true,
              enableSuggestions: hideText == null ? false : true,
              autocorrect: hideText == null ? false : true,
            ),
          ),
        ],
      ),
    );
  }
}
