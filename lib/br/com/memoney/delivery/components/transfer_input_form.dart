import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class TransferInputForm extends StatelessWidget {
  final TextEditingController controller;
  final String title;
  final String hintText;
  final int maxLength;
  final Function(String text) onChanged;
  final EdgeInsetsGeometry padding;
  final TextInputType textInputType;
  final Function(String) onFieldSubmitted;

  TransferInputForm({
    this.controller,
    this.title,
    this.hintText,
    this.maxLength,
    this.onChanged,
    this.padding,
    this.textInputType,
    this.onFieldSubmitted
  });

  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: TextFormField(
        onFieldSubmitted: onFieldSubmitted,
        maxLength: maxLength != null ? maxLength : null,
        controller: controller,
        keyboardType:
            textInputType != null ? textInputType : TextInputType.number,
        textInputAction: TextInputAction.done,
        cursorHeight: 50,
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText != null ? hintText : '0.00',
          hintStyle: GoogleFonts.nunitoSans(
              color: Theme.of(context).primaryColor, fontSize: 36),
        ),
        textAlign: TextAlign.start,
        autofocus: false,
        style: GoogleFonts.nunitoSans(
            color: Theme.of(context).primaryColor, fontSize: 36),
        onChanged: onChanged != null ? onChanged : null,
      ),
    );
  }
}
