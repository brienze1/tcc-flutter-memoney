import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCloseButton extends StatelessWidget {
  final Widget child;
  final Color color;
  final Function onTap;
  final double size;
  final Alignment alignment;
  final EdgeInsetsGeometry padding;

  CustomCloseButton({this.child, this.color, this.onTap, this.size, this.alignment, this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      alignment: alignment != null ? alignment : Alignment.topRight,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          customBorder: new CircleBorder(),
          onTap: onTap != null
              ? onTap
              : () {
                  if (child != null) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return child;
                    }));
                  } else {
                    Navigator.pop(context);
                  }
                },
          child: Icon(
            Icons.close,
            color: color != null ? color : Colors.pink,
            size: size != null ? size : 35,
          ),
        ),
      ),
    );
  }
}
