import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

class MenuItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function onTap;

  MenuItem(this.title, {this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap != null ? onTap : () {},
            child: Container(
              child: Row(
                children: [
                  Icon(icon, color: Theme.of(context).accentColor,),
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(title, style: GoogleFonts.nunitoSans(
                        fontSize: 17, fontWeight: FontWeight.w500, color: Colors.black, decoration: TextDecoration.none),),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}