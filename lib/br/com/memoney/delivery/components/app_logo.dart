import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Container(
            alignment: Alignment.center,
            child: Image.asset(
              'images/logo.png',
              width: 150,
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Container(
          child: Text(
            "MeMoney",
            style: Theme.of(context).textTheme.headline1,
          ),
        )
      ],
    );
  }

}