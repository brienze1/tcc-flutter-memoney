import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_material_pickers/helpers/show_selection_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/delivery/components/transfer_input_form.dart';
import 'package:memoney/br/com/memoney/integration/utils/date_time_zone_brasilia.dart';

import 'close_button.dart';

class CustomBottomSheet extends StatefulWidget {
  final Function onVerticalDragStart;
  final TextEditingController valueController;
  final TextEditingController typeController;
  final TextEditingController dateController;
  final TextEditingController descriptionController;

  const CustomBottomSheet({
    Key key,
    this.onVerticalDragStart,
    this.valueController,
    this.typeController,
    this.dateController,
    this.descriptionController,
  }) : super(key: key);

  @override
  CustomBottomSheetState createState() => CustomBottomSheetState();
}

class CustomBottomSheetState extends State<CustomBottomSheet> {
  String _value = "0.00";
  String _type = "Sent";
  String _date =
      DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTimeBrazilia.now());
  String _description = "New T-shirts";
  String _inputType = "Value";

  @override
  Widget build(BuildContext context) {
    widget.valueController.text = _value;
    widget.typeController.text = _type;
    widget.dateController.text = _date;
    widget.descriptionController.text = _description;

    return GestureDetector(
      onVerticalDragStart: (details) {
        widget.onVerticalDragStart();
      },
      child: Container(
        height: 575,
        child: Column(
          children: [
            Container(
              height: 275,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(24),
                    topRight: Radius.circular(24)),
              ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 16, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Spacer(),
                        Spacer(),
                        Spacer(),
                        Spacer(),
                        Spacer(),
                        Center(
                          child: Text(
                            "Add Transfer",
                            style: GoogleFonts.nunitoSans(
                                fontSize: 27,
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        ),
                        Spacer(),
                        Spacer(),
                        Spacer(),
                        CustomCloseButton(
                          color: Colors.black,
                          padding: const EdgeInsets.only(bottom: 0),
                          size: 25,
                          onTap: () => {
                            widget.onVerticalDragStart(),
                          },
                        ),
                        Spacer()
                      ],
                    ),
                  ),
                  FutureBuilder(
                    future: _getQuestion(),
                    builder: (context,
                        AsyncSnapshot<Map<String, dynamic>> snapshot) {
                      if (snapshot.hasData && snapshot.data != null) {
                        return Column(
                          children: [
                            Container(
                              child: Text(
                                snapshot.data["title"],
                                style: GoogleFonts.nunitoSans(
                                    color: Colors.black, fontSize: 16),
                              ),
                            ),
                            Center(
                              child: Container(
                                  padding: const EdgeInsets.only(top: 44),
                                  child: snapshot.data["input"]),
                            ),
                          ],
                        );
                      }
                      return Column(
                        children: [
                          Container(
                            child: Text(
                              "Please insert the transaction value:",
                              style: GoogleFonts.nunitoSans(
                                  color: Colors.black, fontSize: 16),
                            ),
                          ),
                          Center(
                            child: Container(
                              padding: const EdgeInsets.only(top: 44),
                              child: Text(
                                "0.00",
                                style: GoogleFonts.nunitoSans(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 36),
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              height: 300,
              width: double.infinity,
              color: Color(0xFFEDF1F9),
              child: Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 40, right: 0, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Center(
                          child: Text(
                            "Overview",
                            style: GoogleFonts.nunitoSans(
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _inputType = "Value";
                      setState(() {});
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        child: Text(
                          "Value: " + _value,
                          style: GoogleFonts.nunitoSans(
                              color: Colors.black, fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _inputType = "Type";
                      setState(() {});
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Type: " + _type,
                        style: GoogleFonts.nunitoSans(
                            color: Colors.black, fontSize: 16),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _inputType = "Date";
                      setState(() {});
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Date: " + _date,
                        style: GoogleFonts.nunitoSans(
                            color: Colors.black, fontSize: 16),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _inputType = "Description";
                      setState(() {});
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Description: " + _description,
                        style: GoogleFonts.nunitoSans(
                            color: Colors.black, fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _getQuestionTitle() {
    switch (_inputType) {
      case "Value":
        return "Please insert the transaction value:";
      case "Type":
        return "Please insert the transaction type:";
      case "Date":
        return "Please insert the transaction date:";
      case "Description":
        return "Please insert the transaction description:";
    }
    return "undefined";
  }

  Widget _getQuestionInput() {
    switch (_inputType) {
      case "Value":
        return TransferInputForm(
          textInputType: TextInputType.number,
          hintText: _value,
          onFieldSubmitted: (text) {
            String newValue = double.tryParse(text).toString();
            if (newValue != null && newValue.isNotEmpty) {
              setState(() {
                _value = newValue;
                widget.valueController.text = _value;
              });
            }
            //TODO inserir msg de erro caso esteja vazio
          },
        );
      case "Type":
        return InkWell(
          child: Text(_type,
              style: GoogleFonts.nunitoSans(
                  color: Theme.of(context).primaryColor, fontSize: 36)),
          onTap: () {
            var selected = _type;

            List<String> transferTypes = <String>[
              'Recieved',
              'Sent',
            ];

            List<Icon> icons = <Icon>[
              Icon(
                Icons.arrow_circle_up,
                color: Colors.green,
              ),
              Icon(
                Icons.arrow_circle_down,
                color: Colors.red,
              ),
            ];

            showMaterialSelectionPicker(
              context: context,
              title: "Pick the transfer type",
              items: transferTypes,
              selectedItem: selected,
              icons: icons,
              onChanged: (value) => setState(() => selected = value),
              onConfirmed: () => setState(() {
                _type = selected;
                widget.typeController.text = _type;
              }),
              buttonTextColor: Theme.of(context).primaryColor,
            );
          },
        );
      case "Date":
        return InkWell(
          child: Text(_date,
              style: GoogleFonts.nunitoSans(
                  color: Theme.of(context).primaryColor, fontSize: 36)),
          onTap: () {
            return DatePicker.showDateTimePicker(context,
                theme: DatePickerTheme(
                  containerHeight: 210.0,
                ),
                showTitleActions: true,
                minTime: DateTime(2000, 1, 1),
                maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
              print('confirm $date');
              _date = DateFormat("dd/MM/yyyy HH:mm:ss").format(date);
              widget.dateController.text = _date;
              setState(() {});
            }, currentTime: DateTimeBrazilia.now(), locale: LocaleType.pt);
          },
        );
      case "Description":
        return TransferInputForm(
          textInputType: TextInputType.text,
          hintText: _description,
          onFieldSubmitted: (value) => setState(() {
            _description = value;
            widget.descriptionController.text = _description;
          }),
        );
    }

    return TransferInputForm();
  }

  Future<Map<String, dynamic>> _getQuestion() async {
    Map<String, dynamic> response = new Map();
    response["title"] = _getQuestionTitle();
    response["input"] = _getQuestionInput();
    return response;
  }
}
