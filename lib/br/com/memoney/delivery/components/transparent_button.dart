import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TransparentButton extends StatelessWidget {
  final String title;
  final Function onTap;
  final EdgeInsetsGeometry padding;
  final TextStyle textStyle;

  const TransparentButton({Key key, this.title, this.onTap, this.padding, this.textStyle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null ? padding : const EdgeInsets.all(0.0),
      child: Container(
        child: Material(
          color: Theme.of(context).backgroundColor,
          child: InkWell(
            onTap: onTap != null ? onTap : () {},
            child: Text(
              title != null ? title : "no title",
              style: textStyle != null ? textStyle : Theme.of(context).textTheme.overline,
            ),
          ),
        ),
      ),
    );
  }
}
