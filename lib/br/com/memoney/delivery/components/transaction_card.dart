import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';

class TransactionCard extends StatelessWidget {
  final Transaction transaction;
  final _formatter = new NumberFormat("#,##0.00", "pt_BR");

  TransactionCard({@required this.transaction});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 6),
      height: 80,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(66.0),
        ),
        color: Color(0xFFEDF1F9),
        child: ListTile(
          leading: Icon(
            transaction.tipo == "Sent" ? Icons.arrow_circle_up : Icons.arrow_circle_down,
            color: transaction.tipo == "Sent" ? Colors.red : Colors.green,
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                _valor(transaction.valor),
                style: GoogleFonts.nunitoSans(
                    fontSize: 16, color: Colors.black, fontWeight: FontWeight.w700),
              ),
              Text(
                transaction.tipo,
                style: GoogleFonts.nunitoSans(
                    fontSize: 16, color: transaction.tipo == "Sent" ? Colors.red : Colors.green, fontWeight: FontWeight.w700),
              ),
            ],
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                transaction.descricao,
                style: GoogleFonts.nunitoSans(
                    fontSize: 13, color: Colors.black54, fontWeight: FontWeight.w400),
              ),
              Text(
                DateFormat("MMM d, y").format(transaction.data),
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _valor(double value) {
    String newText = "R\$ " + _formatter.format(value);
    return newText;
  }
}