import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Button extends StatelessWidget {
  final String title;
  final Function onTap;
  final Color color;
  final EdgeInsetsGeometry padding;

  const Button({Key key, this.onTap, this.title, this.color, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null ? padding : const EdgeInsets.all(0.0),
      child: Container(
        width: double.infinity,
        height: 44,
        child: RaisedButton(
          onPressed: onTap != null
              ? onTap
              : () {
                },
          color: color != null ? color : Theme.of(context).accentColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Text(
            title != null ? title : "no title",
            style: Theme.of(context).textTheme.button,
          ),
        ),
      ),
    );
  }
}
