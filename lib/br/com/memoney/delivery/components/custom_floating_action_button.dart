import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/domain/entity/transaction.dart';
import 'package:memoney/br/com/memoney/domain/service/transaction_service.dart';

import 'custom_bottom_sheet.dart';

class MyFloatingActionButton extends StatefulWidget {
  final bool visible;
  final Function onPressed;

  MyFloatingActionButton({this.visible, this.onPressed});

  @override
  _MyFloatingActionButtonState createState() => _MyFloatingActionButtonState();
}

class _MyFloatingActionButtonState extends State<MyFloatingActionButton> {
  bool open = false;
  TextEditingController valueController = new TextEditingController();
  TextEditingController typeController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.visible,
      child: Padding(
        padding: open
            ? const EdgeInsets.only(top: 550)
            : const EdgeInsets.only(top: 0),
        child: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          onPressed: () {
            CustomBottomSheet bottomSheet = CustomBottomSheet(
              valueController: valueController,
              typeController: typeController,
              dateController: dateController,
              descriptionController: descriptionController,
              onVerticalDragStart: () {
                widget.onPressed();
                open = !open;
                setState(() {});
                Navigator.pop(context);
              },
            );

            if (widget.onPressed != null) {
              widget.onPressed();
            }

            if (!open) {
              Scaffold.of(context).showBottomSheet((context) {
                return bottomSheet;
              });
            } else {
              _registerTransaction();
              Navigator.pop(context);
            }
            setState(() {
              open = !open;
            });
          },
          splashColor: Colors.white,
        ),
      ),
    );
  }

  Future<void> _registerTransaction() async {
    try {
      DateTime data = DateFormat("d/MM/yyyy HH:mm:ss").parse(dateController.text);
      Transaction transaction = Transaction(
          valor: double.tryParse(valueController.text),
          data: data,
          descricao: descriptionController.text,
          tipo: typeController.text);
      return await registerNewTransaction(context, transaction);
    } catch (e) {
      debugPrint(e.toString());
      throw e;
    }
  }
}
