import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:memoney/br/com/memoney/delivery/components/custom_floating_action_button.dart';
import 'package:memoney/br/com/memoney/delivery/components/menu_item.dart';
import 'package:memoney/br/com/memoney/delivery/components/transparent_button.dart';
import 'package:memoney/br/com/memoney/delivery/screens/about.dart';
import 'package:memoney/br/com/memoney/delivery/screens/add_transaction.dart';
import 'package:memoney/br/com/memoney/delivery/screens/login.dart';
import 'package:memoney/br/com/memoney/delivery/screens/portfolio.dart';
import 'package:memoney/br/com/memoney/delivery/screens/profile.dart';
import 'package:memoney/br/com/memoney/delivery/screens/regards.dart';
import 'package:memoney/br/com/memoney/delivery/screens/transactions.dart';
import 'package:memoney/br/com/memoney/domain/entity/user.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/active_user_dao.dart';
import 'package:memoney/br/com/memoney/integration/database/dao/user_dao.dart';

class BasicScaffold extends StatefulWidget {
  final Widget body;
  final bool transactionsScreen;
  final bool addTransactionScreen;
  final bool portfolioScreen;
  final bool profileScreen;
  final bool aboutScreen;
  final bool regardsScreen;
  final bool showBottomBar;

  BasicScaffold({
    @required this.body,
    this.transactionsScreen: false,
    this.addTransactionScreen: false,
    this.portfolioScreen: false,
    this.profileScreen: false,
    this.aboutScreen: false,
    this.regardsScreen: false,
    this.showBottomBar: true,
  });

  @override
  _BasicScaffoldState createState() => _BasicScaffoldState();
}

class _BasicScaffoldState extends State<BasicScaffold> {
  final TextStyle _nameFontStyle = GoogleFonts.nunitoSans(
      fontSize: 16, color: Colors.black, fontWeight: FontWeight.w700, decoration: TextDecoration.none);
  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();
  User _user;
  bool _showBottomBar;

  @override
  void initState() {
    super.initState();
    _user = new User(firstName: "User Not Found", lastName: "", validEmail: true);
    _showBottomBar = widget.showBottomBar;
  }

  @override
  Widget build(BuildContext context) {
    return InnerDrawer(
      key: _innerDrawerKey,
      onTapClose: true,
      colorTransitionChild: Colors.transparent,
      colorTransitionScaffold: Colors.transparent,
      offset: IDOffset.only(bottom: 0.03, right: 0.0, left: 0.05),
      scale: IDOffset.horizontal(0.8),
      borderRadius: 35,
      leftChild: Padding(
        padding: const EdgeInsets.only(top: 80, left: 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  Icons.account_circle,
                  size: 65,
                  color: Colors.grey[350],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 30),
                  child: FutureBuilder(
                    future: UserDao.findActiveUser(),
                    builder: (context, AsyncSnapshot<User> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          // TODO: Handle this case.
                          break;
                        case ConnectionState.waiting:
                          // TODO: Handle this case.
                          break;
                        case ConnectionState.active:
                          // TODO: Handle this case.
                          break;
                        case ConnectionState.done:
                          if (snapshot.data != null) {
                            _user = snapshot.data;
                          }
                          break;
                      }
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(toBeginningOfSentenceCase(_user.firstName), style: _nameFontStyle),
                          Text(toBeginningOfSentenceCase(_user.lastName), style: _nameFontStyle),
                          Visibility(
                            child: TransparentButton(
                              padding: const EdgeInsets.only(top: 4),
                              title: "Verify your profile",
                              textStyle: GoogleFonts.nunitoSans(
                                  fontSize: 14, fontWeight: FontWeight.w400, color: Theme.of(context).primaryColor),
                            ),
                            visible: !_user.validEmail,
                          )
                        ],
                      );
                    },
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MenuItem(
                        "Transactions",
                        icon: Icons.autorenew,
                        onTap: widget.transactionsScreen
                            ? () {
                                _innerDrawerKey.currentState.close();
                              }
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return Transactions();
                                }));
                              },
                      ),
                      // MenuItem(
                      //   "Add Transaction",
                      //   icon: Icons.add,
                      //   onTap: widget.addTransactionScreen
                      //       ? () {
                      //           _innerDrawerKey.currentState.close();
                      //         }
                      //       : () {
                      //           Navigator.push(context, MaterialPageRoute(builder: (context) {
                      //             return AddTransaction();
                      //           }));
                      //         },
                      // ),
                      MenuItem(
                        "Portfolio",
                        icon: Icons.home,
                        onTap: widget.portfolioScreen
                            ? () {
                                _innerDrawerKey.currentState.close();
                              }
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return Portfolio();
                                }));
                              },
                      ),
                      // MenuItem(
                      //   "Profile",
                      //   icon: Icons.accessibility_new,
                      //   onTap: widget.profileScreen
                      //       ? () {
                      //           _innerDrawerKey.currentState.close();
                      //         }
                      //       : () {
                      //           Navigator.push(context, MaterialPageRoute(builder: (context) {
                      //             return Profile();
                      //           }));
                      //         },
                      // ),
                      MenuItem(
                        "About",
                        icon: Icons.announcement,
                        onTap: widget.aboutScreen
                            ? () {
                                _innerDrawerKey.currentState.close();
                              }
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return About();
                                }));
                              },
                      ),
                      MenuItem(
                        "Regards",
                        icon: Icons.send,
                        onTap: widget.regardsScreen
                            ? () {
                                _innerDrawerKey.currentState.close();
                              }
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return Regards();
                                }));
                              },
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 32),
              child: MenuItem(
                "Log out",
                icon: Icons.logout,
                onTap: () {
                  ActiveUserDao.clearActiveUser().then((value) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) {
                      return Login();
                    }));
                  });
                },
              ),
            )
          ],
        ),
      ),
      scaffold: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: widget.body,
        bottomNavigationBar: Visibility(
          visible: _showBottomBar,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              topLeft: Radius.circular(20),
            ),
            child: BottomAppBar(
              color: Color(0xFFEDF1F9),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: widget.transactionsScreen
                            ? () {}
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return Transactions();
                                }));
                              },
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Container(
                            height: 50,
                            width: 80,
                            child: Column(
                              children: [
                                Icon(Icons.autorenew,
                                    size: 32,
                                    color:
                                        widget.transactionsScreen ? Theme.of(context).primaryColor : Colors.grey[600]),
                                Text("Transactions",
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: widget.transactionsScreen
                                            ? Theme.of(context).primaryColor
                                            : Colors.grey[600])),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 31),
                      child: Text("New Transfer",
                          style: TextStyle(
                              fontSize: 13,
                              color: widget.addTransactionScreen ? Theme.of(context).primaryColor : Colors.grey[600])),
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: widget.portfolioScreen
                            ? () {}
                            : () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return Portfolio();
                                }));
                              },
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Container(
                            height: 50,
                            width: 80,
                            child: Column(
                              children: [
                                Icon(Icons.home,
                                    size: 32,
                                    color: widget.portfolioScreen ? Theme.of(context).primaryColor : Colors.grey[600]),
                                Text("Portfolio",
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: widget.portfolioScreen
                                            ? Theme.of(context).primaryColor
                                            : Colors.grey[600])),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        floatingActionButton: MyFloatingActionButton(
          visible: widget.showBottomBar,
          onPressed: () {
            _showBottomBar = !_showBottomBar;
            setState(() {});
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked
        ,
      ),
    );
  }
}
