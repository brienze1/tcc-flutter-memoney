import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomToggleButton extends StatelessWidget {
  final List<Widget> children;
  final List<bool> isSelected;
  final Color selectedColor;
  final Color unselectedColor;
  final EdgeInsetsGeometry paddingButton;
  final EdgeInsetsGeometry paddingBetweenButtons;
  final Function(int index) onTap;
  final BorderRadius buttonBorderRadius;

  CustomToggleButton({
    this.children,
    this.isSelected,
    this.selectedColor,
    this.unselectedColor,
    this.paddingButton,
    this.onTap,
    this.paddingBetweenButtons,
    this.buttonBorderRadius,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(
        children.length,
        (int index) {
          return Padding(
            padding: paddingBetweenButtons != null ? paddingBetweenButtons : const EdgeInsets.all(0),
            child: Material(
              borderRadius: buttonBorderRadius != null ? buttonBorderRadius : null,
              color: isSelected[index]
                  ? selectedColor != null
                      ? selectedColor
                      : Theme.of(context).accentColor
                  : unselectedColor != null
                      ? unselectedColor
                      : Colors.transparent,
              child: InkWell(
                onTap: onTap != null
                    ? () {
                        onTap(index);
                      }
                    : null,
                child: Container(
                  padding: paddingButton != null ? paddingButton : const EdgeInsets.all(0),
                  child: children[index],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
