import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'br/com/memoney/config/application_config.dart';
import 'br/com/memoney/delivery/screens/startup.dart';

void main() {
  runApp(MeMoneyApp());
}

class MeMoneyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppConfig(
      apiBaseUrl: "https://r9tskwpeoe.execute-api.sa-east-1.amazonaws.com/prod",
      appName: "MeMoney",
      child: MaterialApp(
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          primaryColor: Colors.pinkAccent,
          primaryColorLight: Color(0xFFEDF1F9),
          accentColor: Colors.grey[400],
          backgroundColor: Colors.white,
          scaffoldBackgroundColor: Colors.white,
          bottomSheetTheme: BottomSheetThemeData(
            backgroundColor: Colors.transparent
          ),

          // Define the default font family.
          fontFamily: 'Georgia',

          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: GoogleFonts.merienda(fontSize: 32, fontWeight: FontWeight.w800, color: Colors.black),
            headline2: GoogleFonts.nunitoSans(fontSize: 27, fontWeight: FontWeight.w800, color: Colors.black),
            headline3: GoogleFonts.nunitoSans(fontSize: 32, fontWeight: FontWeight.w700, color: Colors.white),
            headline4: GoogleFonts.nunitoSans(fontSize: 27, fontWeight: FontWeight.w700, color: Colors.white),
            headline5: GoogleFonts.nunitoSans(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.white),
            headline6: GoogleFonts.nunitoSans(fontSize: 15, color: Colors.grey[400], fontWeight: FontWeight.w600),
            bodyText1: GoogleFonts.nunitoSans(fontSize: 16, color: Colors.black, fontWeight: FontWeight.normal),
            bodyText2: GoogleFonts.nunitoSans(fontSize: 15, color: Colors.grey[400], fontWeight: FontWeight.w600),
            subtitle1: GoogleFonts.nunitoSans(fontSize: 14, fontWeight: FontWeight.w700, color: Colors.white),
            subtitle2: GoogleFonts.nunitoSans(fontSize: 12, fontWeight: FontWeight.w300, color: Colors.white),
            overline: GoogleFonts.nunitoSans(fontSize: 15, color: Colors.pinkAccent, fontWeight: FontWeight.w800),
            button: GoogleFonts.lato(color: Colors.white, fontSize: 16),
            caption: GoogleFonts.nunitoSans(fontSize: 15, color: Colors.grey[400], fontWeight: FontWeight.w600),
          ),
          splashColor: Colors.white,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            splashColor: Colors.white,
            backgroundColor: Colors.pinkAccent,
            hoverColor: Colors.grey[400],
            focusColor: Colors.grey[400],
            foregroundColor: Colors.grey[400],
          ),
        ),
        home: Scaffold(
          body: new Builder(
            builder: (context) {
              return Startup();
            },
          ),
        ),
      ),
    );
  }
}
